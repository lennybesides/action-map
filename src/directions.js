import React, { useRef } from "react";
import { Polyline, useMap } from "react-leaflet";

export default function Directions(props) {
  let map = useMap();
  let polyRef = useRef();
  console.log(polyRef)
  return (
    <Polyline
      ref={polyRef}
      eventHandlers={{
        click: () => map.fitBounds(polyRef.current.getBounds()),
      }}
      positions={props.positions}
      pathOptions={props.pathOptions}
    />
  );
}
