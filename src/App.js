import React, { useEffect, useRef, useState } from "react";
import { MapContainer, TileLayer, Polyline, useMap } from "react-leaflet";
import "./App.css";
import MapMarker from "./marker/MapMarker";
import { Icon } from "leaflet";
import fridgeData from "./mocks/community-fridges";
import Sidebar from "./controls/Sidebar";
import Directions from './directions';

export default function App() {
  const [activePin, setActivePin] = useState(null);
  const [location, setLocation] = useState(null);
  const mapMode = process.env.REACT_APP_MAP_MODE;
  const polyRef = useRef(null);
  const [directions, setDirections] = useState(null);
  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        setLocation([position.coords.latitude, position.coords.longitude]);
      });
    } else {
      setLocation([0, 0]);
    }
    // fetch(
    //   "https://api.geoapify.com/v1/routing?waypoints=" +
    //     encodeURIComponent(
    //       "43.578771,-79.617|43.66847811111111,-79.34063913333334"
    //     ) +
    //     "&mode=drive&apiKey=8fbfbdd734b048c5a3005b632c948c50"
    // )
    //   .then((response) => response.json())
    //   .then((result) => {
    //     let newDirections = result;
    //     let res = result.features[0].geometry.coordinates[0];
    //     let resCoords = res.map((coords) => {
    //       return coords.reverse();
    //     });
    //     newDirections.features[0].geometry.coordinates = resCoords;
    //     setDirections(newDirections);
    //     setLocation(resCoords[resCoords.length / 2]);
    //   })
    //   .catch((error) => console.log("error", error));
  }, []);
  if (location) {
    return (
      <>
        <Sidebar></Sidebar>
        <MapContainer center={location} zoom={12} scrollWheelZoom={false}>
          {mapMode === "mapbox" && (
            <TileLayer
              url={process.env.REACT_APP_MAP_URL}
              attribution="&copy;Mapbox"
            />
          )}

          {directions && (
            <Directions
              positions={directions.features[0].geometry.coordinates}
              pathOptions={{ color: "blue" }}
            />
          )}
          {!mapMode ||
            (mapMode === "osm" && (
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              />
            ))}

          {fridgeData.features.map((feature) => {
            let icon = new Icon({
              iconUrl:
                "https://api.geoapify.com/v1/icon/?type=material&color=blue&icon=" +
                feature.properties.ICON_TYPE +
                "&apiKey=" +
                process.env.REACT_APP_GEOCODE_KEY,
              iconSize: [25, 40],
            });
            if (feature.geometry.coordinates) {
              return (
                <MapMarker
                  key={feature.properties.ID}
                  position={feature.geometry.coordinates}
                  icon={icon}
                />
              );
            }
          })}
        </MapContainer>
      </>
    );
  }
}
