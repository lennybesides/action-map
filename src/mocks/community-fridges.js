
let fridgeData = {
  type: "FeatureCollection",
  crs: {
    type: "name",
    properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" },
  },
  features: [
    {
      type: "Feature",
      properties: {
        ID: 1,
        NAME: "West Mississauga Community Fridge",
        ADDRESS: "40 Dundas street",
        CITY_STATE: "Mississauga ontario",
        ACCESSCTRL: "no/non",
        ACCESSIBLE: "no/non",
        ICON_TYPE: "kitchen",
        OPEN: null,
        NOTES: null,
      },
      geometry: {
        type: "Point",
        coordinates: [43.578771,-79.617],
      },
    },
    {
      type: "Feature",
      properties: {
        ID: 2,
        NAME: "Parkdale Community Fridge",
        ADDRESS: "2246 Bloor street",
        CITY_STATE: "mississauga ontario",
        ACCESSCTRL: "no/non",
        ACCESSIBLE: "no/non",
        ICON_TYPE: "kitchen",
        OPEN: null,
        NOTES: null,
      },
      geometry: {
        type: "Point",
        coordinates: [43.6207087,-79.5802722087403],
      },
    },
    {
      type: "Feature",
      properties: {
        ID: 3,
        NAME: "Brampton Community Fridge",
        ADDRESS: "250 center st",
        CITY_STATE: "mississauga ontario",
        ACCESSCTRL: "no/non",
        ACCESSIBLE: "no/non",
        OPEN: null,
        ICON_TYPE: "kitchen",
        NOTES: null,
      },
      geometry: {
        type: "Point",
        coordinates: [43.6268306,-79.5832597],
      },
    },
    {
      type: "Feature",
      properties: {
        ID: 4,
        NAME: "College Street Community Fridge",
        ADDRESS: "1132 college street",
        CITY_STATE: "toronto ontario",
        ACCESSCTRL: "no/non",
        ACCESSIBLE: "no/non",
        OPEN: null,
        ICON_TYPE: "kitchen",
        NOTES: null,
      },
      geometry: {
        type: "Point",
        coordinates: [43.652505532,-79.433490696],
      },
    },
    {
      type: "Feature",
      properties: {
        ID: 5,
        NAME: "Adelaide Street Community Fridge",
        ADDRESS: "782 adelaide street",
        CITY_STATE: "toronto ontario",
        ACCESSCTRL: "no/non",
        ACCESSIBLE: "no/non",
        OPEN: null,
        ICON_TYPE: "kitchen",
        NOTES: null,
      },
      geometry: {
        type: "Point",
        coordinates: [43.644134,-79.409422],
      },
    },
    {
      type: "Feature",
      properties: {
        ID: 6,
        NAME: "Dundas Street Community Fridge",
        ADDRESS: "555 dundas street east",
        CITY_STATE: "toronto ontario",
        ACCESSCTRL: "no/non",
        ACCESSIBLE: "no/non",
        OPEN: null,
        ICON_TYPE: "kitchen",
        NOTES: null,
      },
      geometry: {
        type: "Point",
        coordinates: [43.659922,-79.362708],
      },
    },
    {
      type: "Feature",
      properties: {
        ID: 7,
        NAME: "Papve Avenue Community Fridge",
        ADDRESS: "348 pape ave",
        CITY_STATE: "toronto ontario",
        ACCESSCTRL: "no/non",
        ACCESSIBLE: "no/non",
        OPEN: null,
        ICON_TYPE: "kitchen",
        NOTES: null,
      },
      geometry: {
        type: "Point",
        coordinates: [43.66847811111111,-79.34063913333334],
      },
    },
  ],
};

fridgeData.features.forEach((feature, index) => {
  if (feature.geometry.coordinates[0]) {
  } else {
    fetch(
      "https://api.geoapify.com/v1/geocode/search?text=" +
        encodeURIComponent(
          feature.properties.ADDRESS + " " + feature.properties.CITY_STATE
        ) +
        "&apiKey=" +
        process.env.REACT_APP_GEOCODE_KEY
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
      });
  }
});
export default fridgeData;
