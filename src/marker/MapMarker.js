import React from "react";
import { Marker, useMap } from "react-leaflet";
import "./MapMarker.css";

export default function MapMarker(props) {
  let map = useMap();
  return (
    <Marker
      eventHandlers={{
        click: () => map.flyTo(props.position, 15, { animate: true }),
      }}
      position={props.position}
      icon={props.icon}
    />
  );
}
