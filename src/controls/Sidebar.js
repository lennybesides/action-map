import { Menu } from "@mui/icons-material";
import {  Drawer, Fab } from "@mui/material";
import { useState } from "react";
import './Sidebar.css'
export default function Sidebar() {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Fab size="small" color="primary" aria-label="menu" onClick={() => setOpen(true)}>
        <Menu></Menu>
      </Fab>
      <Drawer
        anchor="right"
        onOpen={() => setOpen(true)}
        open={open}
        onClose={() => setOpen(false)}
      >
        <p>this is a drawer</p>
      </Drawer>
    </>
  );
}
